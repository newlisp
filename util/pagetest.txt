<br/><br/><br/><h2>Module:&nbsp;mysql5.lsp </h2><p>MySQL v.5 interface</p>
<b>Version: </b>2.6 - addition for mysql_escape_string (Jeff)<br/>
<b>Author: </b>Lutz Mueller 2003-2006, Gordon Fischer 2005, Jeff Ober 2007<br/>
<p></p>
 This MySQL 5 interface module has been tested on version 5.0.19
 of mysql from <a href="http://www.mysql.com">www.mysql.com</a> <p></p>
 This implementation supports a maximum of 2,147,483,647
 rows in a database table. Now automatically adjusts row indexes to
 endian type of host CPU, but higher 32 bits are treated as 0 for now.
<p></p>
 @seealso here here  here
<p></p>
 [h3]Requirements[/h3]
 At the beginning of the program file include a <tt>load</tt> statment for the module:
 [pre]
 (load "/usr/share/newlisp/mysql5.lsp")
 [/pre]
<p></p>
 A version of <tt>libmysqlclient</tt> for a specific platform is required:
<p></p>
 on LINUX/UNIX: <tt>/usr/local/mysql/libmysqlclient.15.so</tt> [br]
 on Mac OS X:   <tt>/usr/local/mysql/libmysqlclient.15.dylib</tt>
<p></p>
 To compile MySQL with client libraries use:
<p></p>
 <tt>./configure --prefix=/usr/local --enable-shared</tt>
<p></p>
 This library might be in a different location on a particular
 installation of MySQL or have a different name.
 Change accordingly in the code at the beginning.
<p></p>
 The MySQL server itself may reside on a different machine
 on the network. The library <tt>libmysqlclient</tt> will communicate
 with that server. The correct connection is created using
 the <tt>MySQL:connect</tt> call.
<p></p>
 At the bottom of the module file <tt>mysql5,lsp</tt> a test routine <tt>test-mysql</tt>
 is included to test for correct installation of MySQL.
<p></p>
 In the <tt>MySQL:connect</tt> call of that test routine the correct parameters 
 for the MySQL server location and user and password have to be inserted.
<p></p>
 [h3]Adapting mysql.lsp to other versions of MySQL[/h3]
 Some of the functions like <tt>mysql_num_rows()</tt> cannot be imported
 because they are really macros extracting data from structures
 like <tt>MYSQL</tt> or <tt>MYSQL_RES</tt>. See the file <tt>mysql.h</tt> in your MySQL distribution.
<p></p>
 The file <tt>sql.c</tt> in the newLISP distribution contains a program
 calculating the offsets of the most important fields in these
 structures. These offsets are used here to retrieve values for
 the number of rows in a result set, etc. Using these offsets
 and the information found in <tt>mysql.h</tt> and <tt>mysql_com.h</tt>, other
 functions can be imported and wrappers built around them.
 In this case one needs to install the developer's version of
 MySQL to get the header files mentioned.
<p></p>
 [h3]Functions available[/h3]
 [pre]
     MySQL:init ................ get a database handle MYSQL
     MySQL:connect ............. connect to a database
     MySQL:query ............... execute a SQL statement
     MySQL:num-rows ............ rows in result of query
     MySQL:num-fields .......... columns in result of query
     MySQL:fetch-row ........... get row from the query result
     MySQL:fetch-all ........... get all rows from the last query
     MySQL:database ............ return all database names
     MySQL:tables .............. return all tables names
     MySQL:fields .............. return all fields in a table
     MySQL:data-seek ........... position in result for fetching
     MySQL:affected-rows ....... number of affected rows from operation
     MySQL:inserted-id ......... last value of auto increment id operation
     MySQL:escape .............. escapes SQL input string using mysql_real_escape_string
     MySQL:error ............... get error message
     MySQL:close-db ............ close database connection
 [/pre]
<p></p>
 [h3]A typical MySQL session[/h3]
 The following code piece outlines a typical MySQL session:
[p][/p][b]example:[/b][blockquote][pre] (load "mysql5.lsp) ; load the module file

 (MySQL:init)       ; initialize
 (MySQL:connect "192.168.1.10" "auser" "secret" "mydb") ; logon
 (MySQL:query "select ...;") ; SQL query
 (MySQL:query "insert ...;") ; SQL query
        ...
 (MySQL:close-db)[/pre][/blockquote]
 The database server is listening on IP 192.168.1.10. The program
 connects with username <tt>"auser"</tt> password <tt>"secret"</tt> to a database with 
 the name <tt>"mydb"</tt>. After connecting SQL statements are performed and
 finally the program disconnects from the server.








<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_init"></a><h3><font color=#CC0000>MySQL:init</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:init</font>)</b><br/>
<p><b>return: </b><tt>true</tt> on success, <tt>nil</tt> on failure.</p>


<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_connect"></a><h3><font color=#CC0000>MySQL:connect</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:connect</font> <em>str-server</em> <em>str-userID</em> <em>str-password</em> <em>str-db</em>)</b><br/>
<b>parameter: </b><em>str-server</em> - The host name or IP address or [tt]0[/tt] for localhost.<br/>
<b>parameter: </b><em>str-userID</em> - The user ID for authentication.<br/>
<b>parameter: </b><em>str-password</em> - The password for authentication.<br/>
<b>parameter: </b><em>str-db</em> - The name of the database to connect to.<br/>
<p><b>return: </b><tt>true</tt> for success or <tt>nil</tt> for failure.</p>
<p></p>
 Connects to a database on server and authenticates a user ID.
 <tt>(MySQL:init)</tt> must have been called previously.


<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_query"></a><h3><font color=#CC0000>MySQL:query</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:query</font> <em>str-sql</em>)</b><br/>
<b>parameter: </b><em>str-sql</em> - A valid SQL query string.<br/>
<p><b>return: </b>For <tt>insert</tt> queries rerturns the inserted ID else <tt>true</tt> </p>
 for success or <tt>nil</tt> for failure.
<p></p>
 Sends a SQL query string to the database server for evaluation.


<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_num-rows"></a><h3><font color=#CC0000>MySQL:num-rows</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:num-rows</font>)</b><br/>
<p><b>return: </b>Number of rows from last query.</p>


<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_num-fields"></a><h3><font color=#CC0000>MySQL:num-fields</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:num-fields</font>)</b><br/>
<p><b>return: </b>Number of columns from last query.</p>





<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_fetch-row"></a><h3><font color=#CC0000>MySQL:fetch-row</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:fetch-row</font>)</b><br/>
<p><b>return: </b>A list of field elements.</p>
<p></p>
 Fetches a row from a previous SQL <tt>MySQL:query</tt>  <tt>select</tt> statement.
 Subsequent calls fetch row by row from the result table until the
 end of the table is reached.


<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_fetch-all"></a><h3><font color=#CC0000>MySQL:fetch-all</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:fetch-all</font>)</b><br/>
<p><b>return: </b>All rows/fields from the last query.</p>
<p></p>
 The whole result set from the query is returned at once as a list of row lists.



<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_databases"></a><h3><font color=#CC0000>MySQL:databases</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:databases</font>)</b><br/>
<p><b>return: </b>A list of databases.</p>
<p></p>
 Performs a <tt>show databases;</tt> query.


<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_table"></a><h3><font color=#CC0000>MySQL:table</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:table</font>)</b><br/>
<p><b>return: </b>A list of tables in the database.</p>
<p></p>
 Performs a <tt>show tables;</tt> query.


<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_fields"></a><h3><font color=#CC0000>MySQL:fields</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:fields</font> <em>str-table</em>)</b><br/>
<b>parameter: </b><em>str-table</em> - The name of the table.<br/>
<p><b>return: </b>A list of field description lists.</p>
<p></p>
 For each field name in the table a list of specifications
 for that field is returned. The list starts with the name
 for the field followed by the type size/precision and
 other optional field descriptions.

  
<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_data-seek"></a><h3><font color=#CC0000>MySQL:data-seek</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:data-seek</font> <em>num-offset</em>)</b><br/>
<b>parameter: </b><em>num-offset</em> - The [tt]0[/tt] based offset to position inside the data set.<br/>
<p><b>return: </b>Always <tt>true</tt>. </p>
<p></p>
 Positions in the result set at a zero based offset
 for a subsequent <tt>MySQL:fetch-row</tt> call. If the offset
 is out of the allowed range for the result set a subsequent
 fetch-row will return <tt>nil</tt>.
 

<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_error"></a><h3><font color=#CC0000>MySQL:error</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:error</font>)</b><br/>
<p><b>return: </b>Text info about the last error which occured.</p>



<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_affected-rows"></a><h3><font color=#CC0000>MySQL:affected-rows</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:affected-rows</font>)</b><br/>
<p><b>return: </b>Number of affected rows by the last <tt>MySQL:query</tt> operation.</p>


<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_inserted-id"></a><h3><font color=#CC0000>MySQL:inserted-id</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:inserted-id</font>)</b><br/>
<p><b>return: </b>Last insert ID from an auto increment field.</p>


<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_escape"></a><h3><font color=#CC0000>MySQL:escape</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:escape</font> <em>str-sql</em>)</b><br/>
<p><b>return: </b>escaped string</p>
<p></p>
 This function will escape special characters in <em>str-sql</em>, so that it 
 is safe to place it in a MySQL query.

<p></p><center>- &sect; -</center><p></p>
<a name="MySQL_close-db"></a><h3><font color=#CC0000>MySQL:close-db</font></h3>
<b>syntax: (<font color=#CC0000>MySQL:close-db</font>)</b><br/>
<p><b>return: </b>Always <tt>true</tt>.</p>
<p></p>
 Closes database access. For new database acess, both <tt>MySQL:init</tt> and 
 <tt>MySQL:connect</tt> functions have to be called.




  

  
  


  
  

  
  

